**********
Quickstart
**********

.. toctree::
   :caption: elsa quickstart
   :maxdepth: 1

   py.rst
   trajectories.rst
   reco.rst
   cxx.rst


Embark on a seamless journey into the world of tomographic reconstruction with
our Quickstart Guides. Whether you are exploring elsa's capabilities in C++ or
Python, these guides are designed to be your navigational compass. Begin by a
complete example, going from zero to full reconstruction in both C++ and
Python. Learn the intricacies of trajectory definition, understanding how to
create trajectories for your specific dataset. Finally, transition to the
Reconstruction Quickstart, where you'll learn how to perform reconstruction
with a variety of different algorithms. You'll learn which algorithm to choose
for your specific application. Each guide is crafted to provide a step-by-step
walkthrough, ensuring a smooth initiation into the art of tomographic
reconstruction with elsa. Choose your path and start unraveling the
possibilities.
