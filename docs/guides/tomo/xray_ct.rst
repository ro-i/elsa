*************************
X-ray Computed Tomography
*************************

X-rays have been discovered By Wilhelm Conrad Röntgen in 1895. Since then
it has transformed many aspects of our lives. Maybe most notably: medical imaging.
The ability to see inside the human body without cutting it open, enabled many
new diagnostics methods. Till today X-ray radiography is a standard procedure
in the clinical routine.

In X-ray radiography, however, scanned objects may superimpose each other and
hence, it might be hard or impossible to distingishe them. X-ray Computed
Tomography (CT) improves on that in many ways. Most notably, it enables 3D
imaging, and hence provides arbitrary slices into the object of interest. This
was previously impossible, and is particullarly important in medical diagnostics,
but also non-destructive testing of materials.

The mathematical foundations for X-ray CT were laid out by Allan Cormack.
However, it did not take of until the publications by Hounsfield, Ambrose and
Perry and Bridges. The idea

The basic principle of X-ray CT is that the object of interest (e.g. a human
body part), lays between the X-ray source and the detector. Then the object
is rotated in respect to the source and detector and multiple X-ray images
are taken from multiple different positions. The most important trajectories are
circular and helical trajectories. I.e. the source and detector move around in
a circular or helical trajectory around the object of interest. The later one
is specifically important in medical imaging.

Now, the challenging part of X-ray CT is to reconstruct the inner part of the
object from the X-ray images (also known as projections).

X-ray CT has been covered by many author, so we do not want to repeat it here.
The book _Computed tomography: algorithms, insight, and just enough theory_ by
Per Christian Hansen, Jakob Jørgensen and William RB Lionheart (2021) is a
great starting point covering many important aspect relevant to fully
understand _elsa_. Another great resource is _Computed Tomography From Photon
Statistics to Modern Cone-Beam CT_ by Thorsten M. Buzug (2008). It is more
focused on classic analytical method, rather than iterative reconstruction, but
it explains many of the underlying principles very well (such as the physical
models). A final recommendation is _Principles of Computerized Tomographic
Imaging_ by Avinash C. Kak and Malcolm Slaney (1988), which is a classic.
