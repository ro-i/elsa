#include "NewtonRaphson.h"
#include "TypeCasts.hpp"

namespace elsa
{

    template <class data_t>
    NewtonRaphson<data_t>::NewtonRaphson(const Functional<data_t>& f, index_t iterations)
        : LineSearchMethod<data_t>(f), iters_(iterations)
    {
    }

    template <class data_t>
    data_t NewtonRaphson<data_t>::solve(DataContainer<data_t> xi, DataContainer<data_t> di)
    {
        auto alpha = data_t{-1.0} * this->_problem->getGradient(xi).dot(di)
                     / di.dot(this->_problem->getHessian(xi).apply(di));

        for (int i = 1; i < iters_; ++i) {
            xi = xi + alpha * di;

            alpha = data_t{-1.0} * this->_problem->getGradient(xi).dot(di)
                    / di.dot(this->_problem->getHessian(xi).apply(di));
        }

        return alpha;
    }

    /// implement the polymorphic comparison operation
    template <class data_t>
    bool NewtonRaphson<data_t>::isEqual(const LineSearchMethod<data_t>& other) const
    {
        auto tmp = downcast_safe<NewtonRaphson<data_t>>(&other);
        return static_cast<bool>(tmp);
    }

    template <class data_t>
    NewtonRaphson<data_t>* NewtonRaphson<data_t>::cloneImpl() const
    {
        return new NewtonRaphson(*this->_problem, iters_);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class NewtonRaphson<float>;
    template class NewtonRaphson<double>;
} // namespace elsa
