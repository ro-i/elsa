#pragma once

#include <optional>

#include "elsaDefines.h"
#include "DataContainer.h"
#include "Cloneable.h"
#include "Functional.h"
#include <cstdint>

namespace elsa
{
    /**
     * @brief Base class representing a line search method for an optimization problem.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     *
     */
    template <typename data_t = real_t>
    class LineSearchMethod : public Cloneable<LineSearchMethod<data_t>>
    {
    public:
        LineSearchMethod(const Functional<data_t>& problem, index_t max_iterations = 10);
        /// default destructor
        ~LineSearchMethod() override = default;

        /**
         * @brief Solve the line search problem (most likely iteratively)
         *
         * @param[in] iterations number of iterations to execute
         * @param[in] x0 optional initial solution, initial solution set to zero if not present
         *
         * @returns the current solution (after solving)
         */
        virtual data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) = 0;

        /// actual comparison method, abstract to force override in derived classes
        virtual bool isEqual(const LineSearchMethod<data_t>& other) const override = 0;

    protected:
        // the differentiable optimizaion problem
        std::unique_ptr<Functional<data_t>> _problem;

        // max number of iterations execute by the line search method
        index_t _max_iterations;
    };
} // namespace elsa
