#include "doctest/doctest.h"

#include "SteepestDescentStepLS.h"
#include "Identity.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "LeastSquares.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("line_search");
TYPE_TO_STRING(SteepestDescentStepLS<float>);
TYPE_TO_STRING(SteepestDescentStepLS<double>);

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("SteepestDescentStepLS: Solving a simple problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a simple problem")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 2;
        VolumeDescriptor dd(numCoeff);

        Eigen::Matrix<data_t, -1, 1> bVec(dd.getNumberOfCoefficients());
        bVec.setRandom();
        DataContainer dcB(dd, bVec);

        Vector_t<data_t> randomData(dd.getNumberOfCoefficients());
        randomData.setRandom();
        DataContainer<data_t> scaleFactors(dd, randomData * static_cast<data_t>(5.14));

        Scaling<data_t> scalingOp(dd, scaleFactors);
        LeastSquares<data_t> prob(scalingOp, dcB);

        WHEN("setting up a SteepestDescentStepLS Line Search and 0s for an initial guess")
        {
            SteepestDescentStepLS<data_t> line_search{prob};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 0;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0453844033054259521353657191866659559309482574462890625,
                            2.250654066514012896504937089048326015472412109375,
                            0.046419260581522354958128318003218737430870532989501953125,
                            0.98461963335424151910757473160629160702228546142578125,
                            0.046709120226465776493096626609258237294852733612060546875,
                            1.0324815057457457090350771977682597935199737548828125,
                            0.04673884349688660344579460570457740686833858489990234375,
                            1.0354896714023398285320354261784814298152923583984375,
                            0.046739998177358910158574190063518472015857696533203125,
                            1.0355559571772550153667680206126533448696136474609375,
                        };
#else
                        solutions = {
                            0.045384403305425959074259623093894333578646183013916015625,
                            2.250654066513977813457358934101648628711700439453125,
                            0.04641926058152230638587099065262009389698505401611328125,
                            0.9846196333542980294595281520741991698741912841796875,
                            0.046709120226465637715218548464690684340894222259521484375,
                            1.032481505745861394274243139079771935939788818359375,
                            0.046738843496886402217871392394954455085098743438720703125,
                            1.035489671402415989831524711917154490947723388671875,
                            0.046739998177358764441802208011722541414201259613037109375,
                            1.035555957177336505736775507102720439434051513671875,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.04538440704345703125,         2.25066089630126953125,
                            0.0464192740619182586669921875, 0.984615266323089599609375,
                            0.046709142625331878662109375,  1.03247165679931640625,
                            0.0467388518154621124267578125, 1.035503387451171875,
                            0.0467399768531322479248046875, 1.035592555999755859375,
                        };
#else
                        solutions = {
                            0.0453844033181667327880859375, 2.2506635189056396484375,
                            0.0464192740619182586669921875, 0.984605848789215087890625,
                            0.04670913517475128173828125,   1.032493114471435546875,
                            0.0467388220131397247314453125, 1.0355002880096435546875,
                            0.04673998057842254638671875,   1.03553545475006103515625,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }
                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }

        WHEN("setting up SteepestDescentStepLS line search and 1s for an intial guess")
        {
            SteepestDescentStepLS<data_t> line_search{prob};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 1;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.045238147717068626085801241742956335656344890594482421875,
                            0.920475490747554747628100813017226755619049072265625,
                            0.045750310612492871287226847698548226617276668548583984375,
                            0.9125877617826809728995840487186796963214874267578125,
                            0.04577418904071887950646413401045720092952251434326171875,
                            0.96222729886465130366701714592636562883853912353515625,
                            0.046098925913740644577654137492572772316634654998779296875,
                            1.185430664332691730322721923585049808025360107421875,
                            0.04644568652429868771402965421657427214086055755615234375,
                            1.2057292841597131438646783863077871501445770263671875,
                        };
#else
                        solutions = {
                            0.045238147717068626085801241742956335656344890594482421875,
                            0.9204754907475549696727057380485348403453826904296875,
                            0.045750310612492871287226847698548226617276668548583984375,
                            0.9125877617826805288103741986560635268688201904296875,
                            0.04577418904071887950646413401045720092952251434326171875,
                            0.96222729886465152571162207095767371356487274169921875,
                            0.046098925913740644577654137492572772316634654998779296875,
                            1.1854306643326915082781169985537417232990264892578125,
                            0.046445686524298708530711365938259405083954334259033203125,
                            1.2057292841595412813404664120753295719623565673828125,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.045238144695758819580078125,  0.920479834079742431640625,
                            0.045750312507152557373046875,  0.9125907421112060546875,
                            0.0457741804420948028564453125, 0.962228357791900634765625,
                            0.0460989661514759063720703125, 1.1854741573333740234375,
                            0.0464456789195537567138671875, 1.20571029186248779296875,
                        };
#else
                        solutions = {
                            0.045238144695758819580078125,  0.920479834079742431640625,
                            0.0457503087818622589111328125, 0.912593185901641845703125,
                            0.0457741878926753997802734375, 0.962234020233154296875,
                            0.0460989437997341156005859375, 1.1854724884033203125,
                            0.04644568264484405517578125,   1.20570600032806396484375,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }
                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }
    }
}
TEST_SUITE_END();
