#include "doctest/doctest.h"

#include "GoldsteinCondition.h"
#include "Identity.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "LeastSquares.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("line_search");
TYPE_TO_STRING(GoldsteinCondition<float>);
TYPE_TO_STRING(GoldsteinCondition<double>);

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("GoldsteinCondition: Solving a simple problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a simple problem")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 2;
        VolumeDescriptor dd(numCoeff);

        Eigen::Matrix<data_t, -1, 1> bVec(dd.getNumberOfCoefficients());
        bVec.setRandom();
        DataContainer dcB(dd, bVec);

        Vector_t<data_t> randomData(dd.getNumberOfCoefficients());
        randomData.setRandom();
        DataContainer<data_t> scaleFactors(dd, randomData * static_cast<data_t>(5.14));

        Scaling<data_t> scalingOp(dd, scaleFactors);
        LeastSquares<data_t> prob(scalingOp, dcB);

        WHEN("setting up a GoldsteinCondition Line Search and 0s for an initial guess")
        {
            GoldsteinCondition<data_t> line_search{prob, 10, 0.25};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 0;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0448754506591455548658586849342100322246551513671875,
                            0.5132228807471264087070039749960415065288543701171875,
                            0.04994083470128264645726545722936862148344516754150390625,
                            0.378815083564082277778339857832179404795169830322265625,
                            0.05036371253877504339246939935037516988813877105712890625,
                            0.45892099926253682173893366780248470604419708251953125,
                            0.0495340111353183287778989551952690817415714263916015625,
                            0.519444557767624104371861903928220272064208984375,
                            0.04849489407102576532526683195101213641464710235595703125,
                            1.0,
                        };
#else

                        solutions = {
                            0.04623359674502924843153550682472996413707733154296875,
                            0.1815813394709999695209035053267143666744232177734375,
                            0.0591021310808222677390943999853334389626979827880859375,
                            0.18574888545953360807772014595684595406055450439453125,
                            0.059505863244932170885714839414504240266978740692138671875,
                            0.1731086454480734726502078046905808150768280029296875,
                            0.0595703125,
                            0.1892858185214371380400422140155569650232791900634765625,
                            0.06080864672364673373383681109771714545786380767822265625,
                            0.1556091143091842543544345289774355478584766387939453125,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0453525520861148834228515625, 2,
                            0.048220090568065643310546875,  0.401290595531463623046875,
                            0.05080313980579376220703125,   0.3286561667919158935546875,
                            0.051060266792774200439453125,  0.383138000965118408203125,
                            0.0502761490643024444580078125, 0.4219563901424407958984375,
                        };
#else
                        solutions = {
                            0.0453525520861148834228515625, 2,
                            0.04667709767818450927734375,   1,
                            0.04784278571605682373046875,   0.5060272216796875,
                            0.0485087446868419647216796875, 0.64638125896453857421875,
                            0.047803364694118499755859375,  1,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }
                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }

        WHEN("setting up GoldsteinCondition line search and 1s for an intial guess")
        {
            GoldsteinCondition<data_t> line_search{prob, 10, 0.25};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 1;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.04528771514832503897185489449839224107563495635986328125,
                            1.0,
                            0.04577775692643552829519393299051444046199321746826171875,
                            0.5691201624818218451906659538508392870426177978515625,
                            0.04817708333333332870740406406184774823486804962158203125,
                            0.35706187102754238793522745254449546337127685546875,
                            0.050541844402328749141783958975793211720883846282958984375,
                            0.28428085149621507010664345216355286538600921630859375,
                            0.052428088092185333446426653836169862188398838043212890625,
                            0.245713184035367848689901393299805931746959686279296875,
                        };
#else

                        solutions = {
                            0.04528771514832503897185489449839224107563495635986328125,
                            1,
                            0.04577775692643552829519393299051444046199321746826171875,
                            0.56891027684616357618097026715986430644989013671875,
                            0.0478515625,
                            0.411442421459847107456653247936628758907318115234375,
                            0.048806436867646872668213831047978601418435573577880859375,
                            0.415727203959100177765861872103414498269557952880859375,
                            0.049641927083333335646297967969076125882565975189208984375,
                            0.329129055468885045598881333717145025730133056640625,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0458984375,
                            0.18598081171512603759765625,
                            0.0575263798236846923828125,
                            0.17529189586639404296875,
                            0.057009138166904449462890625,
                            0.19384436309337615966796875,
                            0.057498343288898468017578125,
                            0.1653645932674407958984375,
                            0.0590628571808338165283203125,
                            0.1653645932674407958984375,
                        };
#else
                        solutions = {
                            0.0458984375,
                            0.187982141971588134765625,
                            0.0563639067113399505615234375,
                            0.1936428844928741455078125,
                            0.05584134161472320556640625,
                            0.2041311562061309814453125,
                            0.054617188870906829833984375,
                            0.224853515625,
                            0.053141273558139801025390625,
                            0.26060903072357177734375,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }

                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }
    }
}
TEST_SUITE_END();
