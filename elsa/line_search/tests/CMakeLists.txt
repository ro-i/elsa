# enable ctest and doctest test discovery
include(CTest)
include(doctest)

# Add custom targets for fine building and running
add_custom_target(build-tests-line_search)
add_custom_target(
    run-tests-line_search
    COMMAND ${CMAKE_CTEST_COMMAND} --output-on-failure --schedule-random
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    DEPENDS build-tests-line_search
    USES_TERMINAL
    COMMENT "Build and run the tests from module 'line_search'."
)

# the actual tests
ELSA_DOCTEST(ArmijoCondition)
ELSA_DOCTEST(GoldsteinCondition)
ELSA_DOCTEST(StrongWolfeCondition)
ELSA_DOCTEST(BarzilaiBorwein)
ELSA_DOCTEST(FixedStepSize)
ELSA_DOCTEST(SteepestDescentStepLS)
ELSA_DOCTEST(NewtonRaphson)
