#pragma once

#include "LineSearchMethod.h"
#include <vector>

namespace elsa
{
    /**
     * @brief Barzilai-Borwein
     *
     * BarzilaiBorwein is an adaptive stepping method based on the soltuion to a two-point
     * approximation of the secant equation to find a step size \alpha. The secant
     * equation:
     *
     * @f[
     * \nabla^2 f_{i+1} s_i = y_i
     * @f]
     *
     * where @f$\nabla^2 f_{i+1}: \mathbb{R}^{n \times n}@f$ the hessian of the
     * objective function @f$f@f$ at @f$x_i@f$,
     * @f$s_i: \mathbb{R}^n is x_{i+1} - x_i@f$ and,
     * @f$y_i: \mathbb{R}^n is \nabla f_{i+1} - \nabla f_i @f$
     *
     *
     * Given the iteration @f$ x_{i+1} = x_i - \alpha \nabla f_i @f$ and the condition
     * @f$ \alpha = argmin_{\alpha}  ||s_i - \alpha y_i||^2 @f$ then we have the
     * BarzilaiBorwein step size:
     *
     * @f[
     * \alpha = \langle s_i, y_i \rangle / \langle y_i, y_i \rangle
     * @f]
     *
     * The biggest drawback of the original implementation of the BarzilaiBorwein is that the setps
     * do not guarantee a descent in the values of the objective function. Which means this method
     * generally does not guarantee convergence to a soultion of the optimization problem. However,
     * in this implementation, which is based on the paper by Marcos Raydan (see references below),
     * we modify the original implementation such that it guarantees that the new value of the
     * objective function satisfies the condition:
     *
     * @f[
     * f(x_i - \alpha \nabla f_i) \le max_{0 \le j \le m} f_{i-j} - \gamma
     * \frac{1}{\alpha} \nabla f_i^T \nabla f_i
     * @f]
     *
     * Which means that the new value of the objective function f has to be less than or
     * equal to the largest value in the last m iterations.
     * Given m greater than zero, this method guarantees convergence to the soltuion of
     * the objective function.
     *
     * References:
     * - Barzilai, Jonathan, and Jonathan M. Borwein. "Two-point step size gradient methods."
     * IMA journal of numerical analysis 8.1 (1988): 141-148.
     * - Raydan, Marcos. "The Barzilai and Borwein gradient method for the large scale
     * unconstrained minimization problem." SIAM Journal on Optimization 7.1 (1997): 26-33.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class BarzilaiBorwein : public LineSearchMethod<data_t>
    {
    public:
        BarzilaiBorwein(const Functional<data_t>& problem, uint32_t m = 10, data_t gamma = 1e-4,
                        data_t sigma1 = 0.1, data_t sigma2 = 0.5, data_t epsilon = 1e-10,
                        index_t max_iterations = 10);
        ~BarzilaiBorwein() override = default;
        data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LineSearchMethod<data_t>& other) const override;

    private:
        // the number of previous objective function values saved in memory
        uint32_t _m;

        // parameter affecting the sufficient decrease condition
        data_t _gamma;

        // factors that determine the next guess of the step size that satisfies the necessary
        // condition
        data_t _sigma1;
        data_t _sigma2;

        // the smallest step size allowed
        data_t _epsilon;

        // the largest step size allowed
        data_t _invepsilon;

        // the last step size
        data_t _li_prev;

        // norm 2 squared of the last search direction
        data_t _derphi_prev;

        // current iteration
        index_t _iter;

        // the last m objective function values
        std::vector<data_t> _function_vals;

        // search direction of the previous step
        DataContainer<data_t> _gi_prev;

        /// implement the polymorphic clone operation
        BarzilaiBorwein<data_t>* cloneImpl() const override;
    };
} // namespace elsa
