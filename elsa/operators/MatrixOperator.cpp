#include "MatrixOperator.h"
#include "Timer.h"
#include "TypeCasts.hpp"
#include "VolumeDescriptor.h"
#include "elsaDefines.h"
#include "thrust/detail/raw_pointer_cast.h"
#include <algorithm>

namespace elsa
{
    template <typename data_t>
    MatrixOperator<data_t>::MatrixOperator(const Matrix_t<data_t>& mat)
        : MatrixOperator<data_t>(VolumeDescriptor({{mat.cols()}}), VolumeDescriptor({{mat.rows()}}),
                                 mat)
    {
    }

    template <typename data_t>
    MatrixOperator<data_t>::MatrixOperator(const DataDescriptor& domain,
                                           const DataDescriptor& range, const Matrix_t<data_t>& mat)
        : LinearOperator<data_t>(domain, range),
          storage_(mat.data(), mat.data() + mat.size()),
          mat_(thrust::raw_pointer_cast(storage_.data()), mat.rows(), mat.cols())
    {
    }

    template <typename data_t>
    void MatrixOperator<data_t>::applyImpl(const DataContainer<data_t>& x,
                                           DataContainer<data_t>& Ax) const
    {
        Timer timeguard("MatrixOperator", "apply");

        if (x.getSize() != this->getDomainDescriptor().getNumberOfCoefficients()) {
            throw Error("MatrixOperator: x needs to be of size {} (is {})",
                        this->getDomainDescriptor().getNumberOfCoefficients(), x.getSize());
        }

        // Wrap data into Eigen Map
        const data_t* ptr = thrust::raw_pointer_cast(x.storage().data());
        Eigen::Map<const Vector_t<data_t>> vec(ptr, mat_.cols());

        Vector_t<data_t> result = mat_ * vec;
        Ax = DataContainer(Ax.getDataDescriptor(), result);
    }

    template <typename data_t>
    void MatrixOperator<data_t>::applyAdjointImpl(const DataContainer<data_t>& y,
                                                  DataContainer<data_t>& Aty) const
    {
        Timer timeguard("MatrixOperator", "applyAdjoint");

        if (y.getSize() != this->getRangeDescriptor().getNumberOfCoefficients()) {
            throw Error("MatrixOperator: y needs to be of size {} (is {})",
                        this->getRangeDescriptor().getNumberOfCoefficients(), y.getSize());
        }

        const data_t* ptr = thrust::raw_pointer_cast(y.storage().data());
        Eigen::Map<const Vector_t<data_t>> vec(ptr, mat_.rows());

        Vector_t<data_t> result = mat_.transpose() * vec;
        Aty = DataContainer(Aty.getDataDescriptor(), result);
    }

    template <typename data_t>
    MatrixOperator<data_t>* MatrixOperator<data_t>::cloneImpl() const
    {
        return new MatrixOperator(this->getDomainDescriptor(), this->getRangeDescriptor(), mat_);
    }

    template <typename data_t>
    bool MatrixOperator<data_t>::isEqual(const LinearOperator<data_t>& other) const
    {
        if (!LinearOperator<data_t>::isEqual(other))
            return false;

        if (!is<MatrixOperator>(other)) {
            return false;
        }

        const auto& otherOp = downcast<MatrixOperator>(other);
        return mat_.isApprox(otherOp.mat_);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class MatrixOperator<float>;
    template class MatrixOperator<double>;

} // namespace elsa
