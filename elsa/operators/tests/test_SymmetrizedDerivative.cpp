/**
 * @file test_SymmetrizedDerivative.cpp
 *
 * @brief Tests for SymmetrizedDerivative class
 */

#include "doctest/doctest.h"
#include "SymmetrizedDerivative.h"
#include "VolumeDescriptor.h"
#include "RandomBlocksDescriptor.h"
#include "testHelpers.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("core");

TEST_CASE_TEMPLATE("SymmetrizedDerivative: Testing construction", data_t, float, double)
{
    GIVEN("a descriptor")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 13, 45;
        VolumeDescriptor dd(numCoeff);

        IndexVector_t numCoeff3(3);
        numCoeff3 << 13, 45, 28;
        VolumeDescriptor dd3(numCoeff3);

        WHEN("instantiating an with a bad descriptor")
        {
            REQUIRE_THROWS_AS(SymmetrizedDerivative<data_t> sd(dd), LogicError);

            IdenticalBlocksDescriptor ibd1(3, dd);
            REQUIRE_THROWS_AS(SymmetrizedDerivative<data_t> sd(ibd1), LogicError);

            IdenticalBlocksDescriptor ibd2(2, dd3);
            REQUIRE_THROWS_AS(SymmetrizedDerivative<data_t> sd(ibd2), LogicError);

            std::vector<std::unique_ptr<DataDescriptor>> dds_list;
            dds_list.emplace_back(dd.clone());
            dds_list.emplace_back(dd3.clone());
            RandomBlocksDescriptor rbd(dds_list);
            REQUIRE_THROWS_AS(SymmetrizedDerivative<data_t> sd(rbd), LogicError);
        }

        IdenticalBlocksDescriptor ibd_domain(2, dd);
        IdenticalBlocksDescriptor ibd_range(3, dd);

        WHEN("instantiating an with a good descriptor")
        {
            SymmetrizedDerivative<data_t> sd(ibd_domain);

            THEN("the DataDescriptors are as expected")
            {
                REQUIRE(sd.getDomainDescriptor() == ibd_domain);
                REQUIRE(sd.getRangeDescriptor() == ibd_range);
            }
        }

        WHEN("cloning an  Identity operator")
        {
            SymmetrizedDerivative<data_t> sd(ibd_domain);
            auto sdClone = sd.clone();

            THEN("everything matches")
            {
                REQUIRE(sdClone.get() != &sd);
                REQUIRE(*sdClone == sd);
            }
        }
    }
}

TEST_CASE_TEMPLATE("SymmetrizedDerivative: Testing apply", data_t, float, double)
{
    GIVEN("some data")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 3;
        VolumeDescriptor dd(numCoeff);
        IdenticalBlocksDescriptor ibd_domain(2, dd);
        IdenticalBlocksDescriptor ibd_range(3, dd);

        SymmetrizedDerivative<data_t> sd(ibd_domain);

        WHEN("applying the symmetrized derivative")
        {
            Vector_t<data_t> data(ibd_domain.getNumberOfCoefficients());
            data << 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6;

            DataContainer<data_t> input(ibd_domain, data);
            auto output = sd.apply(input);

            THEN("the result is as expected")
            {
                Vector_t<data_t> data_expected(ibd_range.getNumberOfCoefficients());
                data_expected << 1, -2, 1, -4, 1, -6, 2, 2, 2, 2, -5, -6, 1.5, 0, 1.5, -1, -2, -6;

                DataContainer<data_t> expected(ibd_range, data_expected);
                REQUIRE(isCwiseApprox(output, expected));
                REQUIRE(output.getDataDescriptor() == ibd_range);
            }
        }
        WHEN("applying the adjoint of symmetrized derivative")
        {
            Vector_t<data_t> data_adj(ibd_range.getNumberOfCoefficients());
            data_adj << 1, 2, 3, 4, 5, 6, 10, 11, 12, 13, 14, 15, 21, 22, 23, 24, 25, 26;

            DataContainer<data_t> input_adj(ibd_range, data_adj);
            auto output_adj = sd.applyAdjoint(input_adj);

            THEN("the result is as expected")
            {
                Vector_t<data_t> data_expected_adj(ibd_domain.getNumberOfCoefficients());
                data_expected_adj << -22, -23, -5, -3, -7, -3, -31, -12, -25, -3, -27, -3;

                DataContainer<data_t> expected_adj(ibd_domain, data_expected_adj);

                REQUIRE(isCwiseApprox(output_adj, expected_adj));
                REQUIRE(output_adj.getDataDescriptor() == ibd_domain);
            }
        }
    }
}
TEST_SUITE_END();
