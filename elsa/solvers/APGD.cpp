#include "APGD.h"
#include "DataContainer.h"
#include "Error.h"
#include "Functional.h"
#include "LeastSquares.h"
#include "LinearOperator.h"
#include "LinearResidual.h"
#include "ProximalL1.h"
#include "TypeCasts.hpp"
#include "Logger.h"
#include "PowerIterations.h"

#include "WeightedLeastSquares.h"
#include "spdlog/stopwatch.h"

namespace elsa
{
    template <typename data_t>
    APGD<data_t>::APGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       const Functional<data_t>& h, std::optional<data_t> mu, data_t epsilon)
        : g_(LeastSquares<data_t>(A, b).clone()),
          h_(h.clone()),
          xPrev_(empty<data_t>(g_->getDomainDescriptor())),
          y_(empty<data_t>(g_->getDomainDescriptor())),
          z_(empty<data_t>(g_->getDomainDescriptor())),
          grad_(empty<data_t>(g_->getDomainDescriptor())),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        if (mu.has_value()) {
            lineSearchMethod_ = FixedStepSize<data_t>(*g_, *mu).clone();
        } else {
            Logger::get("APGD")->info("Computing Lipschitz constant for least squares...");
            // Chose it a little larger, to be safe
            auto L = 1.05 * powerIterations(adjoint(A) * A);
            lineSearchMethod_ = FixedStepSize<data_t>(*g_, 1 / L).clone();
            Logger::get("APGD")->info("Step length chosen to be: {}", 1 / L);
        }

        this->name_ = "APGD";
    }

    template <typename data_t>
    APGD<data_t>::APGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       const DataContainer<data_t>& W, const Functional<data_t>& h,
                       std::optional<data_t> mu, data_t epsilon)
        : g_(WeightedLeastSquares<data_t>(A, b, W).clone()),
          h_(h.clone()),
          xPrev_(empty<data_t>(g_->getDomainDescriptor())),
          y_(empty<data_t>(g_->getDomainDescriptor())),
          z_(empty<data_t>(g_->getDomainDescriptor())),
          grad_(empty<data_t>(g_->getDomainDescriptor())),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        if (mu.has_value()) {
            lineSearchMethod_ = FixedStepSize<data_t>(*g_, *mu).clone();
        } else {
            Logger::get("APGD")->info("Computing Lipschitz constant for least squares...");
            // Chose it a little larger, to be safe
            auto L = 1.05 * powerIterations(adjoint(A) * A);
            lineSearchMethod_ = FixedStepSize<data_t>(*g_, 1 / L).clone();
            Logger::get("APGD")->info("Step length chosen to be: {}", 1 / L);
        }

        this->name_ = "APGD";
    }

    template <typename data_t>
    APGD<data_t>::APGD(const Functional<data_t>& g, const Functional<data_t>& h, data_t mu,
                       data_t epsilon)
        : g_(g.clone()),
          h_(h.clone()),
          xPrev_(empty<data_t>(g_->getDomainDescriptor())),
          y_(empty<data_t>(g_->getDomainDescriptor())),
          z_(empty<data_t>(g_->getDomainDescriptor())),
          grad_(empty<data_t>(g_->getDomainDescriptor())),
          lineSearchMethod_(FixedStepSize<data_t>(*g_, mu).clone()),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        if (!g.isDifferentiable()) {
            throw Error("APGD: g must be differentiable");
        }

        this->name_ = "APGD";
    }

    template <typename data_t>
    APGD<data_t>::APGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       const Functional<data_t>& h,
                       const LineSearchMethod<data_t>& lineSearchMethod, data_t epsilon)
        : g_(LeastSquares<data_t>(A, b).clone()),
          h_(h.clone()),
          xPrev_(empty<data_t>(g_->getDomainDescriptor())),
          y_(empty<data_t>(g_->getDomainDescriptor())),
          z_(empty<data_t>(g_->getDomainDescriptor())),
          grad_(empty<data_t>(g_->getDomainDescriptor())),
          lineSearchMethod_(lineSearchMethod.clone()),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        this->name_ = "APGD";
    }

    template <typename data_t>
    APGD<data_t>::APGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       const DataContainer<data_t>& W, const Functional<data_t>& h,
                       const LineSearchMethod<data_t>& lineSearchMethod, data_t epsilon)
        : g_(WeightedLeastSquares<data_t>(A, b, W).clone()),
          h_(h.clone()),
          xPrev_(empty<data_t>(g_->getDomainDescriptor())),
          y_(empty<data_t>(g_->getDomainDescriptor())),
          z_(empty<data_t>(g_->getDomainDescriptor())),
          grad_(empty<data_t>(g_->getDomainDescriptor())),
          lineSearchMethod_(lineSearchMethod.clone()),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        this->name_ = "APGD";
    }

    template <typename data_t>
    APGD<data_t>::APGD(const Functional<data_t>& g, const Functional<data_t>& h,
                       const LineSearchMethod<data_t>& lineSearchMethod, data_t epsilon)
        : g_(g.clone()),
          h_(h.clone()),
          xPrev_(empty<data_t>(g_->getDomainDescriptor())),
          y_(empty<data_t>(g_->getDomainDescriptor())),
          z_(empty<data_t>(g_->getDomainDescriptor())),
          grad_(empty<data_t>(g_->getDomainDescriptor())),
          lineSearchMethod_(lineSearchMethod.clone()),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        if (!g.isDifferentiable()) {
            throw Error("APGD: g must be differentiable");
        }

        this->name_ = "APGD";
    }

    template <typename data_t>
    DataContainer<data_t> APGD<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, g_->getDomainDescriptor());

        xPrev_ = x;
        y_ = x;
        z_ = x;

        tPrev_ = 1;

        // update gradient
        g_->getGradient(x, grad_);

        // setup done!
        this->configured_ = true;

        return x;
    }

    template <typename data_t>
    DataContainer<data_t> APGD<data_t>::step(DataContainer<data_t> x)
    {
        auto mu = lineSearchMethod_->solve(x, -grad_);
        // z = y - mu_ * grad
        lincomb(1, y_, -mu, grad_, z_);

        // x_{k+1} = prox_{mu * g}(y - mu * grad)
        // x = prox_.apply(z, mu_);
        x = h_->proximal(z_, mu);

        // t_{k+1} = \frac{\sqrt{1 + 4t_k^2} + 1}{2}
        data_t t = (1 + std::sqrt(1 + 4 * tPrev_ * tPrev_)) / 2;

        // y_{k+1} = x_k + \frac{t_{k-1} - 1}{t_k}(x_k - x_{k-1})
        lincomb(1, x, (tPrev_ - 1) / t, x - xPrev_, y_); // 1 temporary

        xPrev_ = x;
        tPrev_ = t;

        // update gradient last
        g_->getGradient(x, grad_);

        return x;
    }

    template <typename data_t>
    bool APGD<data_t>::shouldStop() const
    {
        return grad_.squaredL2Norm() <= epsilon_;
    }

    template <typename data_t>
    std::string APGD<data_t>::formatHeader() const
    {
        return fmt::format("{:^12} | {:^12}", "objective", "gradient");
    }

    template <typename data_t>
    std::string APGD<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        return fmt::format("{:>12.3} | {:>12.3}", g_->evaluate(x) + h_->evaluate(x),
                           grad_.squaredL2Norm());
    }

    template <typename data_t>
    auto APGD<data_t>::cloneImpl() const -> APGD<data_t>*
    {
        return new APGD<data_t>(*g_, *h_, *lineSearchMethod_, epsilon_);
    }

    template <typename data_t>
    auto APGD<data_t>::isEqual(const Solver<data_t>& other) const -> bool
    {
        auto otherAPGD = downcast_safe<APGD>(&other);
        if (!otherAPGD)
            return false;

        if (not lineSearchMethod_->isEqual(*(otherAPGD->lineSearchMethod_)))
            return false;

        if (epsilon_ != otherAPGD->epsilon_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class APGD<float>;
    template class APGD<double>;
} // namespace elsa
