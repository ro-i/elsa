#include "doctest/doctest.h"

#include "Identity.h"
#include "ProximalL1.h"
#include "ALB.h"
#include "Logger.h"
#include "VolumeDescriptor.h"
#include "testHelpers.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("solvers");

TEST_CASE_TEMPLATE("Accelerated Linearized Bregman: Solving a simple linear problem", data_t, float,
                   double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 321);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a linear problem")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 13, 24;
        VolumeDescriptor dd(numCoeff);

        Eigen::Matrix<data_t, -1, 1> bVec(dd.getNumberOfCoefficients());
        bVec.setRandom();

        DataContainer dcB(dd, bVec);

        WHEN("setting up an Accelerated Linearized Bregman solver")
        {
            ALB<data_t> solver{Identity<data_t>(dd), dcB, ProximalL1<data_t>(), 1, 0.5};

            THEN("the clone works correctly")
            {
                auto lbClone = solver.clone();

                CHECK_NE(lbClone.get(), &solver);
                CHECK_EQ(*lbClone, solver);

                AND_THEN("it works as expected")
                {
                    auto solution = solver.solve(100);
                    CHECK_LE((solution - dcB).squaredL2Norm(), doctest::Approx(0).epsilon(0.01));
                }
            }
        }
    }
}

TEST_SUITE_END();
