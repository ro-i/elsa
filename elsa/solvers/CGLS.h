#pragma once

#include <memory>

#include "DataContainer.h"
#include "LinearOperator.h"
#include "Solver.h"
#include "elsaDefines.h"

namespace elsa
{
    /// @brief Conjugate Gradient for Least Squares Problems
    ///
    /// CGLS minimizes:
    /// \f$ \frac{1}{2} || Ax - b ||_2^2 + \eps^2 || x || \f$
    /// where \f$A\f$ is an operator, it does not need to be square, symmetric, or positive
    /// definite. \f$b\f$ is the measured quantity, and \f$\eps\f$ a dampening factors.
    ///
    /// If the dampening factor \f$\eps\f$ is non zero, the problem solves a Tikhonov
    /// problem.
    ///
    /// CGLS is equivalent to apply CG to the normal equations \f$A^TAx = A^Tb\f$.
    /// However, it doesn not need to actually form the normal equation. Primarily,
    /// this improves the runtime performance, and it further improves the stability
    /// of the algorithm.
    ///
    /// References:
    /// - https://web.stanford.edu/group/SOL/software/cgls/
    ///
    /// @author David Frank
    template <typename data_t = real_t>
    class CGLS : public Solver<data_t>
    {
    public:
        /// Scalar alias
        using Scalar = typename Solver<data_t>::Scalar;

        /// @brief Construct the necessary form of CGLS using some linear operator and
        /// the measured data.
        ///
        /// @param A linear operator for the problem
        /// @param b the measured data
        /// @param eps the dampening factor
        /// @param tol stopping tolerance
        CGLS(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
             SelfType_t<data_t> eps = 0, SelfType_t<data_t> tol = 1e-4);

        /// make copy constructor deletion explicit
        CGLS(const CGLS<data_t>&) = delete;

        /// default destructor
        ~CGLS() override = default;

        DataContainer<data_t> setup(std::optional<DataContainer<data_t>> x) override;

        DataContainer<data_t> step(DataContainer<data_t> x) override;

        bool shouldStop() const override;

        std::string formatHeader() const override;

        std::string formatStep(const DataContainer<data_t>& x) const override;

    private:
        std::unique_ptr<LinearOperator<data_t>> A_;

        DataContainer<data_t> b_;

        DataContainer<data_t> r_;

        DataContainer<data_t> s_;

        DataContainer<data_t> c_;

        DataContainer<data_t> q_;

        data_t k_;

        data_t kold_;

        data_t damp_{0};

        data_t tol_{0.0001};

        /// implement the polymorphic clone operation
        CGLS<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Solver<data_t>& other) const override;
    };
} // namespace elsa
