#pragma once

#include "Functional.h"

namespace elsa
{
    /**
     * @brief Class representing the smooth mixed L12 functional.
     *
     * General p,q norm definition:
     * @f[
     * \|A\|_{p, q} = \left(\sum_{j=1}^n \left(\sum_{i=1}^m \left | a_{i j} \right |^p
     * \right)^{\frac{q}{p}} \right)^{\frac{1}{q}}
     * @f]
     *
     * The mixed L21 functional evaluates to
     * @f[
     * \|A\|_{2, 1} = \left( \sum_{j=1}^n\left( \sum_{i=1}^m \left| a_{ij} \right|^2
     * \right)^{\frac{1}{2}} \right)
     * @f]
     *
     */
    template <typename data_t = real_t>
    class MixedL21Norm : public Functional<data_t>
    {
    public:
        explicit MixedL21Norm(const DataDescriptor& domainDescriptor);

        MixedL21Norm(const MixedL21Norm<data_t>&) = delete;

        ~MixedL21Norm() override = default;

        bool isProxFriendly() const override;

        DataContainer<data_t> proximal(const DataContainer<data_t>& v,
                                       SelfType_t<data_t> t) const override;

        void proximal(const DataContainer<data_t>& v, SelfType_t<data_t> t,
                      DataContainer<data_t>& out) const override;

    protected:
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        MixedL21Norm<data_t>* cloneImpl() const override;

        bool isEqual(const Functional<data_t>& other) const override;
    };

} // namespace elsa
