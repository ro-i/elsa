#pragma once

#include "DataContainer.h"
#include "DataDescriptor.h"
#include "Functional.h"

namespace elsa
{
    /// @brief Constant functional. This functinoal maps all input values to a constant scalar
    /// value.
    template <typename data_t = real_t>
    class ConstantFunctional : public Functional<data_t>
    {
    public:
        /// @brief Constructor for the constant functional, mapping domain vector to a scalar
        /// (without a residual)
        ConstantFunctional(const DataDescriptor& descriptor, SelfType_t<data_t> constant);

        /// make copy constructor deletion explicit
        ConstantFunctional(const ConstantFunctional<data_t>&) = delete;

        /// default destructor
        ~ConstantFunctional() override = default;

        bool isDifferentiable() const override;

        bool isProxFriendly() const override;

        /// Return the constant of the functional
        data_t getConstant() const;

        /// The convex conjugate for the constant or zero functional is
        /// @f[
        /// f^*(x)
        /// =
        /// \begin{cases}
        ///     -c, & \text{if } x = 0 \\ \infty, & \text{otherwise}
        /// \end{cases}
        /// @f]
        /// However, in algorithms like PDHG, this usually results in `inf` values,
        /// which is not desirable. Hence, the following penalisation is used:
        /// @f[
        /// f^*(x) = \sum \max(x, 0)
        /// @f]
        data_t convexConjugate(const DataContainer<data_t>& x) const override;

        /// The proximal for any constant function is simply the identity
        DataContainer<data_t> proximal(const DataContainer<data_t>& v,
                                       [[maybe_unused]] SelfType_t<data_t> t) const override;

        /// The proximal for any constant function is simply the identity
        void proximal(const DataContainer<data_t>& v, [[maybe_unused]] SelfType_t<data_t> t,
                      DataContainer<data_t>& out) const override;

    protected:
        /// Return the constant value
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /// The gradient operator is the ZeroOperator, hence set Rx to 0
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        /// There does not exist a hessian, this will throw if called
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /// implement the polymorphic clone operation
        ConstantFunctional<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Functional<data_t>& other) const override;

    private:
        data_t constant_;
    };

    /// @brief Zero functional. This functinoal maps all input values to a zero
    template <typename data_t = real_t>
    class ZeroFunctional : public Functional<data_t>
    {
    public:
        /// @brief Constructor for the zero functional, mapping domain vector to a scalar (without
        /// a residual)
        ZeroFunctional(const DataDescriptor& descriptor);

        /// make copy constructor deletion explicit
        ZeroFunctional(const ConstantFunctional<data_t>&) = delete;

        /// default destructor
        ~ZeroFunctional() override = default;

        bool isDifferentiable() const override;

        bool isProxFriendly() const override;

        /// The convex conjugate for the constant or zero functional is
        /// @f[
        /// f^*(x)
        /// =
        /// \begin{cases}
        ///     -c, & \text{if } x = 0 \\ \infty, & \text{otherwise}
        /// \end{cases}
        /// @f]
        /// However, in algorithms like PDHG, this usually results in `inf` values,
        /// which is not desirable. Hence, the following penalisation is used:
        /// @f[
        /// f^*(x) = \sum \max(x, 0)
        /// @f]
        data_t convexConjugate(const DataContainer<data_t>& x) const override;

        /// The proximal for any constant function is simply the identity
        DataContainer<data_t> proximal(const DataContainer<data_t>& v,
                                       [[maybe_unused]] SelfType_t<data_t> t) const override;

        /// The proximal for any constant function is simply the identity
        void proximal(const DataContainer<data_t>& v, [[maybe_unused]] SelfType_t<data_t> t,
                      DataContainer<data_t>& out) const override;

    protected:
        /// Return the constant value
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /// The gradient operator is the ZeroOperator, hence set Rx to 0
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        /// There does not exist a hessian, this will throw if called
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /// implement the polymorphic clone operation
        ZeroFunctional<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Functional<data_t>& other) const override;
    };

} // namespace elsa
