#include <doctest/doctest.h>

#include "testHelpers.h"
#include "SmoothMixedL21Norm.h"
#include "Identity.h"
#include "VolumeDescriptor.h"
#include "IdenticalBlocksDescriptor.h"
#include "TypeCasts.hpp"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("functionals");

TEST_CASE_TEMPLATE("SmoothMixedL21Norm norm", TestType, float, double)
{

    GIVEN("just data")
    {
        IndexVector_t numCoeff(1);
        numCoeff << 4;
        VolumeDescriptor dd(numCoeff);
        IdenticalBlocksDescriptor ibd(3, dd);

        WHEN("instantiating")
        {
            SmoothMixedL21Norm<TestType> func(ibd, 0.2);

            THEN("the functional is as expected")
            {
                REQUIRE_EQ(func.getDomainDescriptor(), ibd);
            }

            THEN("a clone behaves as expected")
            {
                auto l1Clone = func.clone();

                REQUIRE_NE(l1Clone.get(), &func);
                REQUIRE_EQ(*l1Clone, func);
            }

            THEN("the evaluate, gradient and Hessian work as expected")
            {
                Vector_t<TestType> dataVec(ibd.getNumberOfCoefficients());
                dataVec << 1, 2, 3, 4, 10, 11, -3, -8, -1, -5, 12, 13;
                DataContainer<TestType> dc(ibd, dataVec);

                REQUIRE(checkApproxEq(func.evaluate(dc), 50.8610611860043));

                Vector_t<TestType> dataVecGrad(ibd.getNumberOfCoefficients());
                dataVecGrad << 0.09899535, 0.16327755, 0.23567317, 0.25346934, 0.98995345,
                    0.89802651, -0.23567317, -0.50693868, -0.09899535, -0.40819387, 0.94269267,
                    0.82377536;
                DataContainer<TestType> dcGrad(ibd, dataVecGrad);

                REQUIRE(isCwiseApprox(func.getGradient(dc), dcGrad));
                REQUIRE_THROWS_AS(func.getHessian(dc), LogicError);
            }
        }
    }
}

TEST_SUITE_END();
