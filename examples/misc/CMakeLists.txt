# build the shearlet example program
add_example(shearlet_example shearlet.cpp)
add_example(ndview_ops ndview_ops.cpp)

if(WITH_CUDA)
    include(CheckLanguage)
    check_language(CUDA)
    if(CMAKE_CUDA_COMPILER)
        enable_language(CUDA)
        # build the GPU projector speed test program
        add_example(speed_test speed_test.cpp)

        set_source_files_properties(ndview_ops.cpp PROPERTIES LANGUAGE CUDA)
    endif()
endif()
