import numpy as np
import pyelsa as elsa
import matplotlib.pyplot as plt


def example2d(s: int, show: bool = True):
    size = np.array([s] * 2)
    phantom = elsa.phantoms.modifiedSheppLogan(size)

    tmp = np.rot90(np.asarray(phantom), -1)
    vmin = np.min(tmp)
    vmax = np.max(tmp)
    tmp = ((tmp - vmin) * (1/(vmax - vmin) * 255)).astype('uint8')

    numAngles = 720
    arc = 360
    distance = size[0]
    angles = np.linspace(0, arc, numAngles, endpoint=False)
    sinoDescriptor = elsa.CircleTrajectoryGenerator.trajectoryFromAngles(
        angles, phantom.getDataDescriptor(
        ), distance * 100.0, distance
    )

    projector = elsa.JosephsMethod(phantom.getDataDescriptor(), sinoDescriptor)
    sinogram = projector.apply(phantom)

    h = elsa.IndicatorNonNegativity(phantom.getDataDescriptor())
    solver = elsa.APGD(projector, sinogram, h)
    reconstruction = solver.solve(50)

    if show:
        plt.imshow(np.array(reconstruction), cmap="gray")
        plt.colorbar()
        plt.show()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Process some integers.")
    parser.add_argument("--size", default=128, type=int,
                        help="size of reconstruction")
    parser.add_argument("--no-show", action="store_false")

    args = parser.parse_args()
    show = args.no_show
    example2d(args.size, show)
