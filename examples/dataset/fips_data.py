import numpy as np
from pathlib import Path
import tqdm
import tifffile
import pyelsa as elsa


def rebin2d(arr, binfac):
    """Rebin 2D array arr to shape new_shape by averaging.

    Credits to: https://scipython.com/blog/binning-a-2d-array-in-numpy/
    """

    nx, ny = arr.shape
    bnx, bny = nx // binfac, ny // binfac
    shape = (bnx, arr.shape[0] // bnx, bny, arr.shape[1] // bny)
    return arr.reshape(shape).mean(-1).mean(1)


def preprocess(proj, binning=None, padding=None, cor=0):
    """Preprocessing from transmission to absorption images including some cleaning and correction"""
    # Correct for slight misalignment
    proj = np.roll(proj, cor, axis=1)

    # Take a part of the background and compute an approximation of the
    # initial intensity
    background = proj[:128, :128]
    I0 = np.mean(background)

    # If binning should be performed, do it now
    if binning and binning != 1:
        proj = rebin2d(proj, binning)

    # reduce some noise
    proj[proj > I0] = I0

    # log-transform of projection and rebinning
    proj = -np.log(proj / I0)

    if padding:
        proj = np.pad(proj, padding)

    return proj


def load_dataset(path=Path("."), dataset="walnut", binning=1, step=1, padding=50, angle_factor=2.0):
    print(f"Loading dataset '{dataset}' from {path}")
    # The FIPS datasets all follow the convention of a "<data>_<name>_<angle>.tif
    files = sorted(path.glob(f"*_{dataset}_*.tif"))

    projections = []
    # store angles of each projection we load.
    # This avoids computation later based on step
    angles = []

    for file in tqdm.tqdm(files[:-1:step], desc="Loading and preproecessing"):
        raw = tifffile.imread(file).astype("float32")

        # filename are e.g. 20201111_walnut_XXXX.tif, so split by '_', then by '.'
        filenum = int(str(file).rsplit("_", maxsplit=1)
                      [-1].rsplit(".", maxsplit=1)[0])
        angle = (filenum - 1) / angle_factor
        angles.append(angle)

        projections.append(preprocess(raw, binning=binning, padding=padding))

    # Final step of preprocessing
    projections /= np.max(projections)

    # Change to correct storage order for elsa
    projections = np.transpose(projections, (0, 1, 2))

    return projections, np.array(angles, dtype=np.float32)


def define_sino_descriptor(projections, angles, volume_descriptor, det_spacing, dist_source_origin, dist_origin_detector):
    assert len(angles) == projections.shape[0]

    sino_descriptor = elsa.CircleTrajectoryGenerator.trajectoryFromAngles(
        angles,
        volume_descriptor,
        dist_source_origin,
        dist_origin_detector,
        [0, 0],
        [0, 0, 0],  # Offset of origin
        np.flip(projections.shape[1:]),
        det_spacing,
    )

    return elsa.DataContainer(projections, sino_descriptor)
