@PACKAGE_INIT@

message(STATUS "elsa found at ${CMAKE_CURRENT_LIST_DIR}")
message(STATUS "finding elsa dependencies...")

include(CMakeFindDependencyMacro)
find_dependency(OpenMP QUIET)
find_dependency(Threads REQUIRED) # for spdlog

# Check if CMAKE_CUDA_COMPILER is set
if(EXISTS "@CMAKE_CUDA_COMPILER@")
    enable_language(CUDA)
    find_library(CUDART_LIBRARY cudart ${CMAKE_CUDA_IMPLICIT_LINK_DIRECTORIES})

    find_dependency(CUDAToolkit)
endif()

# when we bundle/git-download these libs we have to ensure that an elsa-downstream project uses the same version, and
# the path to this is encoded in our target info files.
if(@SYSTEM_SPDLOG@)
    # use the system version
    message(STATUS "finding system-version of dependency spdlog...")
    find_dependency(spdlog REQUIRED)
else()
    # use the bundled version
    if("@ELSA_EXPORT_BUILD_TREE@" AND EXISTS "${CMAKE_CURRENT_LIST_DIR}/spdlogTargets.cmake")
        message(STATUS "using elsa build-tree version of dependency spdlog")
        include("${CMAKE_CURRENT_LIST_DIR}/spdlogTargets.cmake")
    else()
        message(STATUS "using elsa-bundled version of dependency spdlog")
        find_dependency(spdlog REQUIRED CONFIG PATHS @CMAKE_INSTALL_PREFIX@ NO_DEFAULT_PATH)
    endif()
endif()

if(@SYSTEM_EIGEN@)
    # use the system version
    message(STATUS "finding system-version of dependency eigen...")
    find_dependency(eigen3 REQUIRED)
else()
    # use the bundled version
    if("@ELSA_EXPORT_BUILD_TREE@" AND EXISTS "${CMAKE_CURRENT_LIST_DIR}/eigen3Targets.cmake")
        message(STATUS "using elsa build-tree version of dependency eigen3")
        include("${CMAKE_CURRENT_LIST_DIR}/eigen3Targets.cmake")
    else()
        # downloaded via CPM
        message(STATUS "using elsa-bundled version of dependency eigen3")

        # We just create it again with the installed files, dirty but it works
        add_library(Eigen INTERFACE)
        add_library(Eigen3::Eigen ALIAS Eigen)

        target_include_directories(
            Eigen SYSTEM INTERFACE @CMAKE_INSTALL_PREFIX@/@CMAKE_INSTALL_INCLUDEDIR@/@EIGEN_BUNDLED_INSTALLDIR@
        )
    endif()
endif()

if(NOT TARGET elsa::Thrust)
    message(STATUS "using Thrust with HOST_SYSTEM=@THRUST_HOST_SYSTEM@ and DEVICE_SYSTEM=@THRUST_DEVICE_SYSTEM@")
    find_dependency(
        Thrust
        REQUIRED
        PATHS
        /opt/cuda
        /usr/local/cuda
        HINTS
        @CMAKE_INSTALL_PREFIX@/lib/cmake/thrust
    )

    # use the host and device system elsa was compiled with!
    thrust_create_target(elsa::Thrust HOST @THRUST_HOST_SYSTEM@ DEVICE @THRUST_DEVICE_SYSTEM@)
endif()

# If FFTW3 is found, we gotta find it again
if("@FFTW3_FOUND@")
    find_package(PkgConfig)
    if(PKG_CONFIG_FOUND)
        pkg_check_modules(FFTW3d IMPORTED_TARGET "fftw3")
        pkg_check_modules(FFTW3f IMPORTED_TARGET "fftw3f")
    else()
        message(FATAL_ERROR "pgk-congfig not found, needed to find FFTW3")
    endif()
endif()

set(_supported_components @ELSA_REGISTERED_COMPONENTS@)

if(NOT elsa_FIND_COMPONENTS)
    # no specific components requested, include all
    message(STATUS "finding elsa_all module")
    find_dependency(elsa_all REQUIRED HINTS ${CMAKE_CURRENT_LIST_DIR})
    if(elsa_all_FOUND)
        include(${CMAKE_CURRENT_LIST_DIR}/elsa_allTargets.cmake)
    endif()
    message(STATUS "elsa found, using all modules")
else()
    # Include specific components
    message(STATUS "finding requested elsa components")
    foreach(_comp ${elsa_FIND_COMPONENTS})
        message(STATUS "elsa loading ${_comp}")
        if(NOT _comp IN_LIST _supported_components)
            set(elsa_FOUND False)
            set(elsa_NOT_FOUND_MESSAGE "Unsupported component: ${_comp}")
        endif()

        find_dependency(elsa_${_comp} REQUIRED)
        if(elsa_${_comp}_FOUND)
            message(STATUS "Including ${_comp}")
            include(${CMAKE_CURRENT_LIST_DIR}/elsa_${_comp}Targets.cmake)
        endif()
    endforeach()
    message(STATUS "elsa found, using components: ${elsa_FIND_COMPONENTS}")
endif()

check_required_components(elsa)
